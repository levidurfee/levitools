<?php

class LeviTools {

	/**
	 * Wraps var_dump in html pre tags
	 * @param mixed $data
	 * @return void
	 */
	public function prettyDump($data) {
		echo "<pre>";
		var_dump($data);
		echo "</pre>";
	}
}
